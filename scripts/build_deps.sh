#!/bin/bash

set -eEo pipefail

install-buildah () {
    . /etc/os-release
    sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${ID^}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
    wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/x${ID^}_${VERSION_ID}/Release.key -O Release.key
    apt-key add - < Release.key
    apt-get update -qq
    apt-get install buildah -qq -y
    sync
    sed -i 's/driver = \"overlay\"/driver = \"vfs\"/g' /etc/containers/storage.conf
}

install-golang () {
    # golang
    wget https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz 
    tar -C /usr/local -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz
    rm go${GOLANG_VERSION}.linux-amd64.tar.gz
    PATH="/usr/local/go/bin:${PATH}"
    go version
}

install-kubectl () {
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x ./kubectl
    mv ./kubectl /usr/local/bin/kubectl
    kubectl version --client
}

install-helm () {
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh
}

install-terraform () {
    wget "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip"
    unzip terraform_${TF_VERSION}_linux_amd64.zip -d /usr/local/bin
    rm terraform_${TF_VERSION}_linux_amd64.zip
}

install-tf-providers () {


    mkdir -p /plugins

	# kubernetes
    wget ${HASHICORP_PROVIDER_URL}/terraform-provider-kubernetes/${KUBERNETES_PROVIDER_VERSION}/terraform-provider-kubernetes_${KUBERNETES_PROVIDER_VERSION}_linux_amd64.zip
    mkdir -p /plugins/registry.terraform.io/hashicorp/kubernetes/${KUBERNETES_PROVIDER_VERSION}/linux_amd64/
    unzip -o terraform-provider-kubernetes_${KUBERNETES_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/kubernetes/${KUBERNETES_PROVIDER_VERSION}/linux_amd64/"
    rm terraform-provider-kubernetes_${KUBERNETES_PROVIDER_VERSION}_linux_amd64.zip

    # helm
    wget ${HASHICORP_PROVIDER_URL}/terraform-provider-helm/${HELM_PROVIDER_VERSION}/terraform-provider-helm_${HELM_PROVIDER_VERSION}_linux_amd64.zip
    mkdir -p /plugins/registry.terraform.io/hashicorp/helm/${HELM_PROVIDER_VERSION}/linux_amd64/
    unzip -o terraform-provider-helm_${HELM_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/helm/${HELM_PROVIDER_VERSION}/linux_amd64/"
    rm terraform-provider-helm_${HELM_PROVIDER_VERSION}_linux_amd64.zip

    # http
    wget ${HASHICORP_PROVIDER_URL}/terraform-provider-http/${HTTP_PROVIDER_VERSION}/terraform-provider-http_${HTTP_PROVIDER_VERSION}_linux_amd64.zip
    mkdir -p /plugins/registry.terraform.io/hashicorp/http/${HTTP_PROVIDER_VERSION}/linux_amd64/
    unzip -o terraform-provider-http_${HTTP_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/http/${HTTP_PROVIDER_VERSION}/linux_amd64/"
    rm terraform-provider-http_${HTTP_PROVIDER_VERSION}_linux_amd64.zip

    # template
    wget ${HASHICORP_PROVIDER_URL}/terraform-provider-template/${TEMPLATE_PROVIDER_VERSION}/terraform-provider-template_${TEMPLATE_PROVIDER_VERSION}_linux_amd64.zip
    mkdir -p /plugins/registry.terraform.io/hashicorp/template/${TEMPLATE_PROVIDER_VERSION}/linux_amd64/
    unzip -o terraform-provider-template_${TEMPLATE_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/template/${TEMPLATE_PROVIDER_VERSION}/linux_amd64/"
    rm terraform-provider-template_${TEMPLATE_PROVIDER_VERSION}_linux_amd64.zip
    
    # linode
    wget ${HASHICORP_PROVIDER_URL}/terraform-provider-linode/${LINODE_PROVIDER_VERSION}/terraform-provider-linode_${LINODE_PROVIDER_VERSION}_linux_amd64.zip
    mkdir -p /plugins/registry.terraform.io/hashicorp/linode/${LINODE_PROVIDER_VERSION}/linux_amd64/
    unzip -o terraform-provider-linode_${LINODE_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/linode/${LINODE_PROVIDER_VERSION}/linux_amd64/"
    rm terraform-provider-linode_${LINODE_PROVIDER_VERSION}_linux_amd64.zip

}


TF_VERSION="1.3.1"
GOLANG_VERSION="1.19.1"
HASHICORP_PROVIDER_URL="https://releases.hashicorp.com"
HELM_PROVIDER_VERSION="2.7.0"
KUBERNETES_PROVIDER_VERSION="2.13.1"
HTTP_PROVIDER_VERSION="3.1.0"
TEMPLATE_PROVIDER_VERSION="2.2.0"
LINODE_PROVIDER_VERSION="1.29.2"


# Installations
install-buildah
install-terraform
install-tf-providers
install-kubectl
install-helm
