#!/bin/bash
# Gitlab runner
# For Debian/Ubuntu/Mint
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
# For Debian/Ubuntu/Mint
export GITLAB_RUNNER_DISABLE_SKEL=true; apt-get -y install gitlab-runner
sync

