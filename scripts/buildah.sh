#!/bin/bash
echo /etc/*-release
. /etc/os-release
sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${ID^}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
wget -nv http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/Release.key -O Release.key
sed -i 's/driver = \"overlay\"/driver = \"vfs\"/g' /etc/containers/storage.conf # Fix overlay issue
apt-key add - < Release.key
apt-get update -qq
apt-get -qq -y install buildah
sync

